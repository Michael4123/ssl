import socket
import mycrypto
from ssl import SSLClient, SSLServer, SSLSocket, recv_min
import ssl
import threading
import struct

open_socks = {}
map_lock = threading.Lock()
		
class Router(SSLServer):
	@staticmethod
	def add_dst(msg, ip, port):
		n_ip = ''.join(map(chr, map(int, ip.split('.'))))
		port = struct.pack('<H', port)
		return n_ip + port + msg
	@staticmethod
	def fetch_dst(msg):
		ip = msg[:4]
		ip = '.'.join(map(str, map(ord, list(ip))))
		port = msg[4:6]
		return (ip, struct.unpack('<H', port)[0]), msg[6:]
	
	@staticmethod
	def is_null_dst(dst):
		if dst[0] == '0.0.0.0' and dst[1] == 0:
			return True
		return False
	def __init__(self, privk, port):
		SSLServer.__init__(self, privk, port)
		
	def accept(self):
		a, b = self.server.accept()
		return RouterHandler(self.pubk_scheme_dec, a)
		
	

class RouterHandler:
	KEEP_ALIVE = 10	# time to keep connection data alive
					# I might want to close the socket but keep 
					# the keys
	def __init__(self, pubk_scheme, sock):
		print 'creation'
		self.server = ssl.SSLServerSession(pubk_scheme, sock)
		self.hshake = False
		self.map_loc = threading.Lock()
		self.socks = {}
	
	def _forw(self):
		msg, cont = self.server.recv()
		dst, msg = Router.fetch_dst(msg)
		if dst not in self.socks:
			self._gen_sock(dst)
				
		self.socks[dst]._send(msg, cont)
		while cont:
			msg, cont = self.server.recv()
			self.socks[dst]._send(msg, cont)
		return dst
	def _back(self, dst):
		cont = True
		while cont:
			msg, cont = self.socks[dst]._recv()
			# with encryption
			self.server.send(msg)
	def forward(self):
		# The functions are not seperated 
		dst = self._forw()
		self._back(dst)
		
	def  _gen_sock(self, dst):
		# Add timer logic
		sock = ssl.Socket.init(dst)
		self.map_loc.acquire()
		self.socks[dst] = sock
		self.map_loc.release()
		
	def close(self):
		for dst, sock in self.socks.iteritems():
			sock.close()
		
	
class RouterInfo(SSLSocket, object):
	@classmethod
	def ToRouter(cls, sock):
		self = cls.__new__(cls)
		SSLSocket.__init__(self)
		# I don't use copy
		self.__dict__.update(sock.__dict__)
		self.dst = ('0.0.0.0', 0)
		return self
	def __init__(self, params, enc_scheme, partner_serial_number):
		SSLSocket.__init__(self)
		self.dst = ('0.0.0.0', 0)
		self.__dict__.update(params.__dict__.copy())
		del self.__dict__['ephermal_key']
		self.enc_scheme = enc_scheme
		self.partner_serial_number = partner_serial_number
		self.my_serial_number = struct.unpack('<H', self.my_serial_number)[0]
	
	def set_dst(self, dst):
		self.dst = dst
		
	def send(self, *args, **kwargs):
		raise NotImplementedError()
		
	def recv(self, *args, **kwargs):
		raise NotImplementedError()
		
		
		
class OnionClient(SSLClient):
		
	def __init__(self):
		self.routers = []
		self.sync = False
	
	def _unpack_onion(self, msg):
		for rout in self.routers:
			# the full protocol
			msg = rout.dec(msg)
		return msg
		
	def recv(self):
		msg, cont = SSLClient.recv(self)
		return self._unpack_onion(msg), cont
	def _recv_handshake_resp(self):
		# cont MUST be false, and so is discarded
		in_msg, cont = self._recv()
		in_msg = self._unpack_onion(in_msg)
		
		iv, in_msg = in_msg[:SSLSocket.IV_LEN], in_msg[SSLSocket.IV_LEN:]
		return iv, in_msg
		
	def enc(self, msg):
		return msg
		
	def dec(self, msg):
		return msg
		
	def _send(self, msg, cont = False):
		print 'at _send', len(msg)
		if self.sync:
			# backward packing
			for rout in reversed(self.routers):
				msg = Router.add_dst(msg, rout.dst[0], rout.dst[1])
				msg = rout.enc(msg)

		SSLClient._send(self, msg, cont)
	
	def set_dst(self, dst, pk = None):
		self.routers[-1].set_dst(dst)
		
	def add_router(self, dst, pk):
		
		if not self.sync:
			SSLClient.__init__(self, pk)
			self.connect(dst[0], dst[1])
			self.sync = True
			self.routers.append(RouterInfo.ToRouter(self))
		else:
			self.routers[-1].set_dst(dst)
			
			self.pubk_scheme_enc = lambda msg : mycrypto.PKCS1_OAP_enc.enc(pk, msg)
			
			params, enc_scheme, psn = self._handshake_free()
			self.routers.append(RouterInfo(params, enc_scheme, psn))
			
		
		
		
		
		
			
		
	
	
	
	
	
	
		