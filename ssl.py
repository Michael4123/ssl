import mycrypto
import socket
import sys, time
import struct
from Crypto import Random
from threading import Thread, Lock
import logging


def recv_min(sock, n):
	time_to_wait = 0.01
	if n <= 0:
		return	# raise? too big? TODO.
	s = ''
	while len(s) < n:
		s += sock.recv(n - len(s))
		time.sleep(time_to_wait)
		time_to_wait *= 2
	return s
	
class Socket:
	@classmethod
	def init(cls, dst):
		self = cls()
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.settimeout(10)
		self.sock.connect(dst)
		return self
		
	def _send(self, msg, more_packets = False):
		msg_len = len(msg)
		if more_packets:
			msg_len |= (1 << 15)
			
		msg_len = struct.pack('<H', msg_len)
		self.sock.send(msg_len + msg)
		
	def _recv(self):
		msg_len = struct.unpack('<H', recv_min(self.sock, 2))[0]
		
		cont = (msg_len & (1 << 15)) != 0	# True if there are more packets
		msg_len &= ((1 << 15) - 1)	# turn off msb
		
		return recv_min(self.sock, msg_len), cont
		
	def close(self):
		self.sock.close()
		
class SSLSocket(Socket):
	SSL_VERSION = 1
	AUTH_OK = 0
	
	# sad overhead
	HMAC_OUTPUT_SIZE = 32
	MSG_SERIAL_LEN = 2
	SESSION_ID_LEN = 16		#interpreted as string
	EPHERMAL_KEY_LEN = 32	# birthday attack consists of 2^(16*8) = 2^128, which is secure.
	KEY_LEN = 16
	IV_LEN = KEY_LEN
	MAX_SIZE = 1024
	
	#TODO change?
	FIN_MSG = '\xff\xff\xff\xff'
	FIN_MSG_SERV = 'ABBA'
	
	RSA_LEN = 256
	def __init__(self):
		self.mac_scheme = None
		self.closed = False
		
	def enc(self, m):
		if self.mac_scheme == None:
			self.mac_scheme = mycrypto.SHA256Mac(self.key)
		msg = struct.pack('<H', self.my_serial_number) + self.session_id
		m = msg + m
		m = self.enc_scheme.encrypt(m)
		mac = self.mac_scheme.Mac(m)
		
		self.my_serial_number = (self.my_serial_number + 1) % (1 << 16)
		return m + mac
	
	def _unpack_msg(self, msg):
		mac = msg[-SSLSocket.HMAC_OUTPUT_SIZE:]
		iv = msg[:SSLSocket.IV_LEN]
		
		msg = self.enc_scheme.decrypt(msg[SSLSocket.IV_LEN:-SSLSocket.HMAC_OUTPUT_SIZE], iv)
		
		i = 0
		# first item - my serial number
		psn = struct.unpack('<H', msg[:SSLSocket.MSG_SERIAL_LEN])[0] % (1 << 16)
		i += SSLSocket.MSG_SERIAL_LEN
		# second item - session id
		session_id = msg[i:i+SSLSocket.SESSION_ID_LEN]
		i += SSLSocket.SESSION_ID_LEN
		
		return mac, psn, session_id, msg[i:]
	def dec(self, msg):
		orig_msg = msg[:-SSLSocket.HMAC_OUTPUT_SIZE]
		mac, psn, session_id, msg = self._unpack_msg(msg)
		if self.mac_scheme.Vrfy(orig_msg, mac) == False:
			logging.error('wrong mac')
			return
		
		if psn != self.partner_serial_number:
			logging.error('wrong serial number')
			return
		
		if session_id != self.session_id:
			logging.error('wrong session id')
			return
			
		self.partner_serial_number = (self.partner_serial_number + 1) % (1 << 16)
		
		return msg
	# not thread safe
	def send(self, m, more_packets = False):
		if len(m) == 0:
			return
		elif len(m) >= SSLSocket.MAX_SIZE:
			# Note - 128 is an ESTIMATE to the auxiliary
			# parameters. it is obviously less than that
			self.send(m[:SSLSocket.MAX_SIZE-128], True)
			self.send(m[SSLSocket.MAX_SIZE-128:])
		
		m = self.enc(m)
		self._send(m, more_packets)
		
	# not thread safe
	def recv(self):
		if self.mac_scheme == None:
			self.mac_scheme = mycrypto.SHA256Mac(self.key)
			
		msg, cont = self._recv()
			
		msg = self.dec(msg)
		if msg == None:
			logging.error('forgery')
			# raise forgery exception
			return

		return msg, cont
		
	def close(self):
		raise NotImplementedError()
		
class SSLServer(SSLSocket):
	# Thread safe
	def __init__(self, privk_file, port, lis = 5):
		SSLSocket.__init__(self)
		self.pubk_scheme_dec = lambda ctxt : mycrypto.PKCS1_OAP_enc.dec(privk_file, ctxt)
		self._gen(port, lis)
		
	def _gen(self, port, lis):
		self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.server.bind(('', port))
		self.server.listen(lis)
		
	def accept(self):
		a, b = self.server.accept()
		a.settimeout(10)
		return SSLServerSession(self.pubk_scheme_dec, a)
			
class SSLServerSession(SSLServer):
	def __init__(self, pubk_scheme, sock):
		self.closed = False
		self.mac_scheme = None
		self.pubk_scheme_dec = pubk_scheme
		self.sock = sock
		self._handshake()
		
	def _unpack_handshake(self, msg):
		i = 0
		# first unpacked item - session id
		self.session_id = msg[i:i+SSLSocket.SESSION_ID_LEN]
		i += SSLSocket.SESSION_ID_LEN

		# second unpacked item - partner serial number
		self.partner_serial_number = msg[i:i+SSLSocket.MSG_SERIAL_LEN]
		self.partner_serial_number = struct.unpack('<H', self.partner_serial_number)[0]
		i += SSLSocket.MSG_SERIAL_LEN
		
		# third unpacked item - ephermal key
		ephermal_key = msg[i:i+SSLSocket.EPHERMAL_KEY_LEN]	# easier and more secure
		i += SSLSocket.EPHERMAL_KEY_LEN
		
		# fourth unpacked item - init vector
		self.iv = msg[i:i+SSLSocket.IV_LEN]
		i += SSLSocket.IV_LEN
		
		# fifth unpacked item - key
		self.key = msg[i:i+SSLSocket.KEY_LEN]
		
		return ephermal_key
	# not thread safe
	def _handshake(self):
		# recieve a message with pk encrpytion of 
		# random string, private key, and session id
		
		# even though we don't yet share keys I use
		# The native API
		msg, cont = self._recv()	# always 256 bytes
		print len(msg), cont
		msg = self.pubk_scheme_dec(msg)
		
		ephermal_key = self._unpack_handshake(msg)
		
		self.mac_scheme = mycrypto.SHA256Mac(self.key)
		# Reveive ended
		self.enc_scheme = mycrypto.AESEnc(self.key)
		self.my_serial_number = Random.new().read(2)
		
		out_msg = ephermal_key + self.session_id + self.my_serial_number

		self._send(self.enc_scheme.encrypt(out_msg))
		self.my_serial_number = struct.unpack('<H', self.my_serial_number)[0]
		
		print 'hand shake:' ,self.my_serial_number, self.partner_serial_number
		return True
		
	def close(self):
		self.closed = True
		self.sock.close()
		
	
class HandshakeParams:
	def __init__(self):
		self.session_id = Random.new().read(SSLSocket.SESSION_ID_LEN)
		self.my_serial_number = Random.new().read(SSLSocket.MSG_SERIAL_LEN)
		self.iv = Random.new().read(SSLSocket.IV_LEN)
		self.key = Random.new().read(SSLSocket.KEY_LEN)
		self.ephermal_key = Random.new().read(SSLSocket.EPHERMAL_KEY_LEN)
		
	def _make_hshake_msg(self):
		return self.session_id + self.my_serial_number + self.ephermal_key + self.iv + self.key
		
		
class SSLClient(SSLSocket):
	def __init__(self, pk_file):
		SSLSocket.__init__(self)
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.pubk_scheme_enc = lambda msg : mycrypto.PKCS1_OAP_enc.enc(pk_file, msg)
		
	def connect(self, ip, port):
		self.sock.connect((ip, port))
		self.sock.settimeout(10)
		self._handshake()
	
	def _unpack_handshake(self, in_msg):
		i = 0
		# first item unpacked - ephermal key
		ephermal_key_in = in_msg[i:SSLSocket.EPHERMAL_KEY_LEN]
		i += SSLSocket.EPHERMAL_KEY_LEN
		
		# second item unpacked - session id
		session_id = in_msg[i:i+SSLSocket.SESSION_ID_LEN]

		i += SSLSocket.SESSION_ID_LEN
		
		print i, len(in_msg)
		# second item unpacked - partner serial number (it has to be 2 bytes)
		partner_serial_number = struct.unpack('<H', in_msg[i:i+SSLSocket.MSG_SERIAL_LEN])[0]
		
		return ephermal_key_in, session_id, partner_serial_number
	
	def _recv_handshake_resp(self):
		in_msg, cont = self._recv()
		iv, in_msg = in_msg[:SSLSocket.IV_LEN], in_msg[SSLSocket.IV_LEN:]
		return iv, in_msg
	
	def _handshake_free(self):
		params = HandshakeParams()
		enc_scheme = mycrypto.AESEnc(params.key)
		out = params._make_hshake_msg()
		
		# only once, should be overriden
		out = self.pubk_scheme_enc(out)
		# this turns out to be the encryption
		self._send(out) 
		
		iv, in_msg = self._recv_handshake_resp()
		in_msg = enc_scheme.decrypt(in_msg, iv)
		
		ephermal_key_in, session_id, partner_serial_number = self._unpack_handshake(in_msg)
		
		if params.ephermal_key != ephermal_key_in:
			logging.error('forgery')
			# forgery
			# raise
			return

		if params.session_id != session_id:
			logging.error('wrong session id')
			# wierd problem
			return

		return params, enc_scheme, partner_serial_number
		
	def _handshake(self):
		val = self._handshake_free()
		if val is None:
			return False
		
		self.enc_scheme = val[1]
		
		self.my_serial_number = struct.unpack('<H', val[0].my_serial_number)[0]
		self.session_id = val[0].session_id
		self.partner_serial_number = val[2]
		self.key = val[0].key
		print 'hand shake:' ,self.my_serial_number, self.partner_serial_number
		
		return True
	def close(self):
		if self.sock.closed:
			return
		# TODO close scheme
		self.sock.closed = True
		self.sock.close()
		
		
		
		