import ssl
import socket, struct
from threading import Thread
import sys


dst_ip = sys.argv[1] 		# ip to connect
dst_port = int(sys.argv[2]) # port to connect
lis_port = int(sys.argv[3])	# port to listen
pubk_path = sys.argv[4]

MAX_SIZE_PACKET = 800

def forward(sock):

	msg_len = struct.unpack('<H', ssl.recv_min(sock, 2))[0]
	msg = ssl.recv_min(sock, msg_len)

	# debug - print msg
	
	sec_sock = ssl.SSLClient(pubk_path)
	sec_sock.connect(dst_ip, dst_port)
		
	sec_sock.send(msg)
	
	cont = True
	while cont:
		msg, cont = sec_sock.recv()
		sock.send(msg)
	
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', lis_port))
s.listen(5)

while True:
	a, b = s.accept()
	Thread(target=forward, args=(a,)).start()
	
	
	
	
	