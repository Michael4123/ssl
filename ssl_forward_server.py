import ssl
import socket, struct
from threading  import Thread
import sys


dst_ip = sys.argv[1] 		# ip to forward
dst_port = int(sys.argv[2]) # port to forward
lis_port = int(sys.argv[3])	# port to listen
privk_path = sys.argv[4]

MAX_SIZE_PACKET = 800

def forward(sec_sock):
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect((dst_ip, dst_port))
	
	# receiving from secure socket
	# forwarding to regular socket
	cont = True
	while cont: 
		m, cont = sec_sock.recv()
		s.send(m)
	
	# receiving from regular socket
	# forwarding to secure socket
	cont = True
	while cont:
		msg_len = struct.unpack('<H', ssl.recv_min(s, 2))[0]
		cont = (msg_len & (1 << 15)) != 0
		msg_len &= ((1 << 15) - 1)	# turn off msb

		msg = ssl.recv_min(s, msg_len)
		sec_sock.send(msg, cont)
		

	
s = ssl.SSLServer(privk_path, lis_port)

while True:
	x = s.accept()
	Thread(target=forward,args=(x,)).start()
	
	