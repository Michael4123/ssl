import routers, ssl
import time, sys, os, os.path
import socket, struct
from base64 import b64encode

DIR_NAME = 'public_keys'

ADD_ROUT = 1
ADD_PK = 2
ADD_DST = 3
SEND = 4

# TODO
PORT = 9224

r = routers.OnionClient()
closed = False
socks = {}
def decode(sock):
	code = struct.unpack('<H', ssl.recv_min(sock, 2))[0]
	print code
	if code == ADD_ROUT:
		add_rout(sock)
	elif code == ADD_PK:
		add_pk(sock)
	elif code == ADD_DST:
		add_dst(sock)
	elif code == SEND:
		send(sock)

def get_dst(sock):
	dst = ssl.recv_min(sock, 6)
	return dst
	
def strToDst(st):
	print 'strToDst'
	if len(st) != 6:
		return None
	port = struct.unpack('<H', st[4:])[0]
	ip = '.'.join(map(str, map(ord, list(st[:4]))))
	return (ip, port)
	
def add_rout(sock):
	print 'add_rout'
	# get destination
	dst = get_dst(sock)
	if dst in socks and socks[dst] == False:
		sock.send('\x02')	# Failure
		return
	r.add_router(strToDst(dst), pk_file(dst))
	sock.send('\x01')
	
def add_pk(sock):
	dst = get_dst(sock)
	# receive pk file len
	# unfortunately the strlen is 4
	len = struct.unpack('<I', ssl.recv_min(sock, 4))[0]
	pk_cont = ssl.recv_min(sock, len)
	sock.send('\x01')
	if dst in socks and socks[dst]:
		return	# pk already exists
	pk_path = pk_file(dst)
	if os.path.isfile(pk_path):
		socks[dst] = True
		return 
	f = open(pk_file(dst), 'w')
	f.write(pk_cont)
	f.close()
	socks[dst] = True

def pk_file(dst):
	return DIR_NAME + '/' + b64encode(dst) + '.pem'
	
def add_dst(sock):
	dst = get_dst(sock)
	print 'add_dst', strToDst(dst)
	r.set_dst(strToDst(dst))
	sock.send('\x01')
	
def send(sock):
	global closed
	print 'send'
	msg_len = struct.unpack('<I', ssl.recv_min(sock, 4))[0]
	r.send(ssl.recv_min(sock, msg_len))
	msg, cont = r.recv()
	sock.send(struct.pack('<I', len(msg)) + msg)
	a.close()
	closed = True
	
	
# Assuming no race condition
if not os.path.exists(DIR_NAME):
    os.makedirs(DIR_NAME)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', PORT))
# This socket serves only one client
s.listen(1)
a, b = s.accept()

while not closed:
	decode(a)

