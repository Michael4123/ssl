import os
import sys
import struct
import time

from socket import *
from multiprocessing import Process
from threading import Thread

processes = []
print os.getpid()
def new_process(name, *params):
	print 'new process!', os.getpid()
	args = ['python']
	args.append(name + '.py')
	for i in params:
		args.append(str(i))
		
	os.system(' '.join(args))

	
# arg 1 is router_num (=2 for now)
# arg 2 is first router
# arg 3 is second router
# arg 4 is port to listen at
if __name__ == '__main__':
	processes.append(Process(target=new_process, args=('client',)))
	processes.append(Process(target=new_process, args=('router','privk.pem',sys.argv[1])))
	processes.append(Process(target=new_process, args=('router','privk.pem',sys.argv[2])))
	processes.append(Process(target=new_process, args=('router','privk.pem',sys.argv[3])))
	processes.append(Process(target=new_process, args=('basic_server',sys.argv[4])))
	
	for p in processes:
		p.start()
		
	while True:
		processes[0].join()
		processes[0] = Process(target=new_process, args=('client',))
		processes[0].start()
	
	
	
	