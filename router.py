import routers
import sys, csv
import threading

# sys arguments:
#		1 - conf file
#		2 - port

def han_conn(sslrout):
	try:
		while True:
			sslrout.forward()
	except:
		pass
			
def main():
	# TODO add configuration file
	r = routers.Router(sys.argv[1], int(sys.argv[2]))
	
	# I can set a manual limit, but there is no need
	while True:
		x = r.accept()
		threading.Thread(target=han_conn, args=(x,)).start()
	
if __name__ == '__main__':
	main()