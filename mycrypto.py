from Crypto.Hash.SHA256 import SHA256Hash
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto import Random
'''
Cryptographic oracle
'''

def pad_len(msg, dst_len = 16):
	
	msg = msg + 1
	msg += ((dst_len - (msg % dst_len)) % dst_len)
	return msg
def pad(msg, dst_len = 16):	#padding bytes, not bits
	msg += '\xff'
	msg += '\x00'*((dst_len - (len(msg) % dst_len)) % dst_len)
	return msg
	
def unpad(msg):
	i = len(msg) - 1
	while msg[i] == '\x00':
		i -= 1
	return msg[0:i]

def get_random_bytes(n):
	s = Random.new()
	return s.read(n)
	
class PlainRSA:
	@staticmethod
	def Gen(n):
		n = max(1024, n)
		n -= (n % 256)
		key = RSA.generate(n)
		#p,q are unnecessary
		#n is very important
		#d, e are the sk and pk
		return key.n, key.d, key.e
	@staticmethod
	def Enc(m, d, n):
		return pow(m, d, n)	#m in the power of d modolu n
	@staticmethod
	def Dec(c, e, n):
		return pow(c, e, n)	#c in the power of e mod new
							#c is m^d mod n, so c^e = m^ed = m (amazing!)
class Encryption:	#AES
	def __init__(self, key):
		self.key = key
		
	def Enc(self, msg):	
		raise NotImplementedError()
		
	def Dec(self, ciphertext):
		raise NotImplementedError()
		
class AESEnc(Encryption):
	def __init__(self, key, iv = '\x00'*AES.block_size):
		self.iv = iv
		Encryption.__init__(self, key)
		
	def encrypt(self, msg):
		iv = self.iv # TODO change
		# I always pad, because I don't want to add 'padded' state
		msg = pad(msg)
		cipher = AES.new(self.key, AES.MODE_CFB, iv)
		self.iv = self.next_iv(iv)
		return iv + cipher.encrypt(msg)

	
	def decrypt(self, ctxt, iv):
		cipher = AES.new(self.key, AES.MODE_CFB, iv)
		ptxt = cipher.decrypt(ctxt)
		ptxt = unpad(ptxt)
		return ptxt
		
	def next_iv(self, iv):
		return iv

class MAC:	
	def __init__(self, key, blocksize, hashf):
		self.key = key
		self.blocksize = blocksize
		self.hash = hashf
		self._hmac_init()
	def Mac(m):
		raise NotImplementedError()
	
	def Vrfy(s, m):
		raise NotImplementedError()
	
	def _hmac_init(self):
		# len of number should be well-defined
		if len(self.key) > self.blocksize: 
			#SHA256 is my hash function 
			self.key = self.hash(self.key) # keys longer than blocksize are shortened
			
		if len(self.key) < self.blocksize:
			# keys shorter than blocksize are zero-padded
			self.key = self.key + '\x00'*(self.blocksize - len(self.key))
			
		self.o_key_pad = MAC._str_xor('\x5c'*self.blocksize, self.key)
		self.i_key_pad = MAC._str_xor('\x36'*self.blocksize, self.key)
	#Could be static method
	
	@staticmethod
	def _str_xor(s1, s2):
		if len(s1) != len(s2):
			return
		x = zip(map(ord, list(s1)), map(ord, list(s2)))
		x = map(lambda x: x[0]^x[1], x)
		x = map(chr, x)
		return ''.join(x)
		
class SHA256Mac(MAC):
	def __init__(self, key):
		MAC.__init__(self, key, 32, lambda msg: SHA256Hash(msg).digest())
	def Mac(self, m):
		return self.hash(self.o_key_pad + self.hash(self.i_key_pad + m)) 
	def Vrfy(self, msg, mac):
		return self.Mac(msg) == mac
		
	
class PKCS1_OAP_enc:
	@staticmethod
	def enc(key, message):
		key = RSA.importKey(open(key).read())
		cipher = PKCS1_OAEP.new(key)
		ciphertext = cipher.encrypt(message)
		return ciphertext
	@staticmethod
	def dec(key, ctxt):
		key = RSA.importKey(open(key).read())
		cipher = PKCS1_OAEP.new(key)
		message = cipher.decrypt(ctxt)
		return message
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
